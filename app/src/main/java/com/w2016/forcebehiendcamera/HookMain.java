package com.w2016.forcebehiendcamera;

import static de.robv.android.xposed.XposedBridge.hookAllMethods;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class HookMain  implements IXposedHookLoadPackage {
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        //XposedBridge.log(" has Hooked!");
        Class clazz = loadPackageParam.classLoader.loadClass("android.hardware.Camera");
        //Class clazz = XposedHelpers.findClass("android.hardware.Camera", loadPackageParam.classLoader);
        XposedHelpers.findAndHookMethod(clazz, "open", int.class,new XC_MethodHook() {
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                param.args[0]=0;
                XposedBridge.log("Camera has hooked");
            }

        });

        Class clazz1 = loadPackageParam.classLoader.loadClass("android.hardware.camera2.CameraManager");
        //Class clazz1 = XposedHelpers.findClass("android.hardware.camera2.CameraManager", loadPackageParam.classLoader);
        XposedHelpers.findAndHookMethod(clazz1, "openCamera", String.class,android.hardware.camera2.CameraDevice.StateCallback.class,android.os.Handler.class,new XC_MethodHook() {
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                param.args[0]="0";
                XposedBridge.log("Camera2 has hooked");
            }

        });

    }

}